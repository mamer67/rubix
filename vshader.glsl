# version 130

in vec4 vPosition;
in vec3 vertexColor;

//varying vec4 vPosition;
//varying vec3 vertexColor;


out vec3 fragmentColor;
//varying vec3 fragmentColor;

// Values that stay constant for the whole mesh
uniform mat4 mvp;

void
main()
{
    gl_Position = mvp*vPosition;
    
    // The color of each vertex will be interpolated
	// to produce the color of each fragment
	fragmentColor = vertexColor;
}

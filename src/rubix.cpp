//============================================================================
// Name        : rubix.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "include/Angel.h"

#include <iostream>
#include <cstdlib>
using namespace std;

// window dimension
GLfloat window_width = 800;
GLfloat window_height = 500;

// cube dimension
const GLfloat side = 0.6;
const GLfloat gap = 0.015;

/**
 * list of points to draw cube, at least need 4 vertices for each side, as each
 * side has unique color we need 24 vertex to draw cube, can't reuse vertices to
 * use only 8 vertices as color per vertix not unique
 *
 * first 3 points are location, next 3 points are color
 */
const GLfloat poff[] = {
//front face -red
		-side / 2, -side / 2, side / 2, 1.0f, 0.0f, 0.0f, //0
		side / 2, -side / 2, side / 2, 1.0f, 0.0f, 0.0f, //1
		side / 2, side / 2, side / 2, 1.0f, 0.0f, 0.0f, //2
		-side / 2, side / 2, side / 2, 1.0f, 0.0f, 0.0f, //3

		//right face -yellow
		side / 2, -side / 2, side / 2, 1.0f, 1.0f, 0.0f, //4
		side / 2, -side / 2, -side / 2, 1.0f, 1.0f, 0.0f, //5
		side / 2, side / 2, -side / 2, 1.0f, 1.0f, 0.0f, //6
		side / 2, side / 2, side / 2, 1.0f, 1.0f, 0.0f, //7

		//back face -orange
		-side / 2, -side / 2, -side / 2, 1.0f, 0.5f, 0.0f, //8
		side / 2, -side / 2, -side / 2, 1.0f, 0.5f, 0.0f, //9
		side / 2, side / 2, -side / 2, 1.0f, 0.5f, 0.0f, //10
		-side / 2, side / 2, -side / 2, 1.0f, 0.5f, 0.0f, //11

		//left face -white
		-side / 2, -side / 2, side / 2, 1.0f, 1.0f, 1.0f, //12
		-side / 2, -side / 2, -side / 2, 1.0f, 1.0f, 1.0f, //13
		-side / 2, side / 2, -side / 2, 1.0f, 1.0f, 1.0f, //14
		-side / 2, side / 2, side / 2, 1.0f, 1.0f, 1.0f, //15

		//bottom face -green
		-side / 2, -side / 2, side / 2, 0.0f, 1.0f, 0.0f, //16
		side / 2, -side / 2, side / 2, 0.0f, 1.0f, 0.0f, //17
		side / 2, -side / 2, -side / 2, 0.0f, 1.0f, 0.0f, //18
		-side / 2, -side / 2, -side / 2, 0.0f, 1.0f, 0.0f, //19

		//up face -blue
		-side / 2, side / 2, side / 2, 0.0f, 0.0f, 1.0f, //20
		side / 2, side / 2, side / 2, 0.0f, 0.0f, 1.0f, //21
		side / 2, side / 2, -side / 2, 0.0f, 0.0f, 1.0f, //22
		-side / 2, side / 2, -side / 2, 0.0f, 0.0f, 1.0f, //23
		};

/*
 * This array tells how to order the vertices to draw the triangles of the cube
 * this array is important since we are reusing vertices, we could discard the array
 * at a cost of adding 12 more vertices so total is 36 vertices
 */
const GLuint _order[] = {
//front face
		0, 1, 2 //
		, 2, 3, 0 //

		//right face
		, 4, 5, 6 //
		, 6, 7, 4 //

		//back face
		, 8, 9, 10 //
		, 10, 11, 8 //

		//left face
		, 12, 13, 14 //
		, 14, 15, 12 //

		//bottom face
		, 16, 17, 18 //
		, 18, 19, 16 //

		//up face
		, 20, 21, 22 //
		, 22, 23, 20 //
		};

const int _win_face[6][9] = { //
		{ 0, 1, 2, 3, 4, 5, 6, 7, 8 }, //up
				{ 18, 19, 20, 21, 22, 23, 24, 25, 26 }, //bottom
				{ 0, 3, 6, 9, 12, 15, 18, 21, 24 }, //left
				{ 2, 5, 8, 11, 14, 17, 20, 23, 26 }, //right
				{ 0, 1, 2, 9, 10, 11, 18, 19, 20 }, //front
				{ 6, 7, 8, 15, 16, 17, 24, 25, 26 } //back
		};

// hint axes
const GLfloat hb = 1.6f;
const GLfloat hs = 1.2f;

// random number shuffle
const int rtimes = 15;
const int rdelay = 20;

// theta displacement when do rotate around x,y,z
const int theta = 1;
// factor to slow rotation
const GLfloat factor = 3.0;

// angle stp in twisting single faces
const GLfloat anglest = 25;
// delay between twisting steps
const int timemsec = 100;

GLuint v_buffer, color_buffer, v_o_buffer;
GLuint slocation;
GLint obj_color;
GLint transformation;
mat4 projection, view, model, mvp;
vec4 camera;

GLfloat points[4000];
GLuint vertex_order[1000];
mat4 rot[27];
int slots[27];

vec2 drag_s_point;
mat4 rotate; // for whole cube rotation

bool animate_d = false;
bool random_d = false;
bool pause_d = false;

GLfloat tmpa;
int randm__cnt = 0;

GLfloat angle; // to know clockwise or anti clockwise
int num; // to know row, column

// Display function ---------------------------------------------------------
void init(void) {

	GLfloat offs = gap + side;
	GLfloat tmpx, tmpy, tmpz;
	int indx = 0;
	int eindx = 0;
	int start;

	tmpy = offs;
	for (int y = 0; y < 3; ++y) {

		tmpz = -offs;
		for (int z = 0; z < 3; ++z) {

			tmpx = -offs;
			for (int x = 0; x < 3; ++x) {
				start = indx / 6;

				//loop on all offsets of cube
				for (int k = 0; k < 24; ++k) {
					bool color = false;

					points[indx++] = poff[k * 6 + 0] + tmpx;
					points[indx++] = poff[k * 6 + 1] + tmpy;
					points[indx++] = poff[k * 6 + 2] + tmpz;

					if (tmpx < 0 && (k >= 12 && k <= 15)) {
						//left
						color = true;
					} else if (tmpx > 0 && (k >= 4 && k <= 7)) {
						//right
						color = true;
					}

					if (tmpy < 0 && (k >= 16 && k <= 19)) {
						//bottom
						color = true;
					} else if (tmpy > 0 && (k >= 20 && k <= 23)) {
						//up
						color = true;
					}

					if (tmpz < 0 && (k >= 8 && k <= 11)) {
						//back
						color = true;
					} else if (tmpz > 0 && (k >= 0 && k <= 3)) {
						//front
						color = true;
					}

					if (color) {
						points[indx++] = poff[k * 6 + 3];
						points[indx++] = poff[k * 6 + 4];
						points[indx++] = poff[k * 6 + 5];
					} else {
						points[indx++] = 0;
						points[indx++] = 0;
						points[indx++] = 0;
					}
				}

				//assign right order
				for (int k = 0; k < 36; ++k) {
					vertex_order[eindx++] = start + _order[k];
				}

				//increment offset
				tmpx += offs;
			}

			tmpz += offs;
		}

		tmpy -= offs;
	}

// draw z axis
	start = indx / 6;
	points[indx++] = 0.0f;
	points[indx++] = 0.0f;
	points[indx++] = -hs;
	points[indx++] = 0.0f;
	points[indx++] = 0.0f;
	points[indx++] = 1.0f;
	points[indx++] = 0.0f;
	points[indx++] = 0.0f;
	points[indx++] = hb;
	points[indx++] = 0.0f;
	points[indx++] = 0.0f;
	points[indx++] = 1.0f;

	vertex_order[eindx++] = start + 0;
	vertex_order[eindx++] = start + 1;

// draw y axis
	start = indx / 6;
	points[indx++] = 0.0f;
	points[indx++] = hb;
	points[indx++] = 0.0f;
	points[indx++] = 1.0f;
	points[indx++] = 1.0f;
	points[indx++] = 1.0f;
	points[indx++] = 0.0f;
	points[indx++] = -hs;
	points[indx++] = 0.0f;
	points[indx++] = 1.0f;
	points[indx++] = 1.0f;
	points[indx++] = 1.0f;

	vertex_order[eindx++] = start + 0;
	vertex_order[eindx++] = start + 1;

// draw x axis
	start = indx / 6;
	points[indx++] = -hs;
	points[indx++] = 0.0f;
	points[indx++] = 0.0f;
	points[indx++] = 1.0f;
	points[indx++] = 0.0f;
	points[indx++] = 0.0f;
	points[indx++] = hb;
	points[indx++] = 0.0f;
	points[indx++] = 0.0f;
	points[indx++] = 1.0f;
	points[indx++] = 0.0f;
	points[indx++] = 0.0f;

	vertex_order[eindx++] = start + 0;
	vertex_order[eindx++] = start + 1;

// initialize slots
	for (int i = 0; i < 27; ++i) {
		slots[i] = i;
	}

// create a vertex array object
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

// create and initialize a vertex buffer object
	glGenBuffers(1, &v_buffer);
	glBindBuffer( GL_ARRAY_BUFFER, v_buffer);
	glBufferData( GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);

// create vertex_order_buffer to save the order of the vertices
	glGenBuffers(1, &v_o_buffer);
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, v_o_buffer);
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(vertex_order), vertex_order, GL_STATIC_DRAW);

// load shaders and use the resulting shader program
	GLuint program = InitShader("vshader.glsl", "fshader.glsl");
	glUseProgram(program);

// initialize the vertex position attribute from the vertex shader
	slocation = glGetAttribLocation(program, "vPosition");
	obj_color = glGetAttribLocation(program, "vertexColor");
	transformation = glGetUniformLocation(program, "mvp");

// bind the slocation attribute with the vertices of the buffer
	glEnableVertexAttribArray(slocation);
	glBindBuffer(GL_ARRAY_BUFFER, v_buffer);
	glVertexAttribPointer(slocation, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
	BUFFER_OFFSET(0));

// bind the obj_color attribute to the colors attached to the vertex
	glEnableVertexAttribArray(obj_color);
	glVertexAttribPointer(obj_color, 3,
	GL_FLOAT,
	GL_FALSE, 6 * sizeof(GLfloat),
	BUFFER_OFFSET(3*sizeof(GLfloat)));

// projection matrix : 45° Field of View, 4:3 ratio, display range :0.1 unit <-> 100 units
// review Amin_view_transfo to know about perspective matrix
	projection = Perspective(45.0f, window_width / window_height, 0.1f, 100.0f);

	/**
	 * we position the camera at 3,3,3 try changing this and see what happens
	 * we keep looking at the origin, COI, center of interest, as all our drawing in origin, w.r.t origin
	 * we look up,
	 */
	camera = vec4(3, 3, 3, 1);
	view = LookAt(camera, // Camera is at (4,3,-3), in World Space
			vec4(0, 0, 0, 1),  // and looks at the origin
			vec4(0, 1, 0, 1) // Head is up (set to 0,-1,0 to look upside-down)
					);

// Model matrix : an identity matrix (model will be at the origin)

//glClearColor(1.0, 1.0, 1.0, 1.0); // white background
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

//---------------------------------------------------------------------------
void display(void) {
// clear the window
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

// draw cubes
	for (int i = 0; i < 27; i++) {
		mvp = projection * view * rotate * rot[i] * model;

		glUniformMatrix4fv(transformation, 1, GL_TRUE, mvp);
		glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, BUFFER_OFFSET(36*i*sizeof(GLuint)));
	}

// set the transformation
	mvp = projection * view * rotate * model;
	glUniformMatrix4fv(transformation, 1, GL_TRUE, mvp);
	glDrawElements(GL_LINES, 6, GL_UNSIGNED_INT, BUFFER_OFFSET(36*27*sizeof(GLuint)));

// swap buffers since we use double buffer
	glutSwapBuffers();
}

int evaluate(int type, int val) {
	if (type == 0) {
		//x axis
		return (val / 3) * 9 + (val % 3) * 3;
	} else if (type == 1) {
		//y axis
		return val;
	} else {
		//z axis
		return (val / 3) * 9 + (val % 3);
	}
}

int find_face(int num, int after) {

	for (int i = 0; i < 6; ++i) {
		for (int j = 0; j < 9; ++j) {
			if (_win_face[i][j] == num && i > after)
				return i;
		}
	}

	return -1;
}

bool find(int fc, int num) {
	for (int i = 0; i < 9; ++i) {
		if (_win_face[fc][i] == num)
			return true;
	}
	return false;
}

bool iswinner() {
	bool fc[6] = { 0, 0, 0, 0, 0, 0 };

	int fs;

	cout << "check up" << endl;

	// check up
	fs = find_face(slots[evaluate(1, 0)], -1);
	do {
		if (fc[fs] == false) {
			cout << "face " << fs << endl;
			int j;
			bool mark[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			for (j = 0; j < 9; ++j) {
				int ind = evaluate(1, j);
				cout << "face " << fs << find(fs, slots[ind]) << endl;
				if (find(fs, slots[ind]) && !mark[j]) {
					mark[j] = true;
				} else {
					break;
				}
			}

			if (j == 9) {
				fc[fs] = true;
				break;
			}
			cout << "infiniteloop" << endl;
		}
	} while ((fs = find_face(slots[evaluate(1, 0)], fs)) > -1);

	cout << "check down" << endl;

	// check bottom
	fs = find_face(slots[18 + evaluate(1, 0)], -1);
	do {
		if (fc[fs] == false) {
			cout << "face " << fs << endl;
			bool mark[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			int j;
			for (j = 0; j < 9; ++j) {
				int ind = 18 + evaluate(1, j);

				if (find(fs, slots[ind]) && !mark[j]) {
					mark[j] = true;
				} else {
					break;
				}
			}

			if (j == 9) {
				fc[fs] = true;
				break;
			}
		}
	} while ((fs = find_face(slots[18 + evaluate(1, 0)], fs)) > -1);

	cout << "check left" << endl;

// check left
	fs = find_face(slots[evaluate(0, 0)], -1);
	do {
		if (fc[fs] == false) {
			cout << "face " << fs << endl;
			bool mark[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			int j;
			for (j = 0; j < 9; ++j) {
				int ind = evaluate(0, j);

				if (find(fs, slots[ind]) && !mark[j]) {
					mark[j] = true;
				} else {
					break;
				}
			}

			if (j == 9) {
				fc[fs] = true;
				break;
			}
		}
	} while ((fs = find_face(slots[evaluate(0, 0)], fs)) > -1);

	cout << "check right" << endl;

// check right
	fs = find_face(slots[2 + evaluate(0, 0)], -1);
	do {
		if (fc[fs] == false) {
			cout << "face " << fs << endl;
			bool mark[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			int j;
			for (j = 0; j < 9; ++j) {
				int ind = 2 + evaluate(0, j);

				if (find(fs, slots[ind]) && !mark[j]) {
					mark[j] = true;
				} else {
					break;
				}
			}

			if (j == 9) {
				fc[fs] = true;
				break;
			}
		}
	} while ((fs = find_face(2 + slots[evaluate(0, 0)], fs)) > -1);

	cout << "check front" << endl;

// check front
	fs = find_face(slots[evaluate(2, 0)], -1);
	do {
		if (fc[fs] == false) {
			cout << "face " << fs << endl;
			bool mark[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			int j;
			for (j = 0; j < 9; ++j) {
				int ind = evaluate(2, j);

				if (find(fs, slots[ind]) && !mark[j]) {
					mark[j] = true;
				} else {
					break;
				}
			}

			if (j == 9) {
				fc[fs] = true;
				break;
			}
		}
	} while ((fs = find_face(slots[evaluate(2, 0)], fs)) > -1);

	cout << "check back" << endl;

// check back
	fs = find_face(slots[6 + evaluate(2, 0)], -1);
	do {
		if (fc[fs] == false) {
			cout << "face " << fs << endl;
			bool mark[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			int j;
			for (j = 0; j < 9; ++j) {
				int ind = 6 + evaluate(2, j);

				if (find(fs, slots[ind]) && !mark[j]) {
					mark[j] = true;
				} else {
					break;
				}
			}

			if (j == 9) {
				fc[fs] = true;
				break;
			}
		}
	} while ((fs = find_face(slots[6 + evaluate(2, 0)], fs)) > -1);

	// after
	cout << "after " << slots[6 + evaluate(2, 0)] << "  " << find_face(slots[evaluate(2, 0)], 0) << endl;
	cout << fs << endl;

	for (int i = 0; i < 6; ++i)
		cout << fc[i] << "  ";
	cout << endl;

	for (int i = 0; i < 6; ++i) {
		if (fc[i] == false) {
			return false;
		}
	}

	return true;
}

void transpose(int offs, int type) {
	int M = 3;
	int N = 3;
	int mn = M * N; /* M rows and N columns */

	int q = mn - 1;

	int i = 0; /* Index of 1D array that represents the matrix */
	int k;
	int tmp;

	do {

		k = (i * M) % q;

		while (k > i)
			k = (M * k) % q;

		if (k != i) {
			//Swap(k, i);
			tmp = slots[offs + evaluate(type, k)];
			slots[offs + evaluate(type, k)] = slots[offs + evaluate(type, i)];
			slots[offs + evaluate(type, i)] = tmp;
		}

	} while (++i <= (mn - 2));
}

void swap_rows(int offs, int type) {
	int tmp;
	int step = evaluate(type, 1) - evaluate(type, 0);

	for (int i = 0; i < 3; ++i) {
		tmp = slots[offs + evaluate(type, i)];
		slots[offs + evaluate(type, i)] = slots[offs + evaluate(type, 6) + i * step];
		slots[offs + evaluate(type, 6) + i * step] = tmp;
	}
}

//rows are counted from 0-2 starting from +ve x direction i.e most right
void dorotate_x(GLfloat angl) {

	int indx;
	if (num == 0) {
		for (int j = 0; j < 3; j++) {
			for (int i = 0; i < 9; i += 3) {
				indx = slots[2 + j * 9 + i];
				rot[indx] = RotateX(angle * angl) * rot[indx];
			}
		}
	} else if (num == 1) {
		for (int j = 0; j < 3; j++) {
			for (int i = 0; i < 9; i += 3) {
				indx = slots[1 + j * 9 + i];
				rot[indx] = RotateX(angle * angl) * rot[indx];
			}
		}
	} else {
		for (int j = 0; j < 3; j++) {
			for (int i = 0; i < 9; i += 3) {
				indx = slots[j * 9 + i];
				rot[indx] = RotateX(angle * angl) * rot[indx];
			}
		}
	}

	glutPostRedisplay();
}

void check_win() {
	if (iswinner()) {
		cout << "user wins" << endl;
		pause_d = true;
	}
}

void finish_x() {
	int strt;

// change direction
	if (num == 0) {
		strt = 2;
	} else if (num == 1) {
		strt = 1;
	} else {
		strt = 0;
	}

// reassign slots
	if (angle > 0) {
		//anti clockwise
		swap_rows(strt, 0);
		transpose(strt, 0);

	} else {
		//clockwise
		transpose(strt, 0);
		swap_rows(strt, 0);
	}
}

//rows are counted from 0-2 starting from +ve y direction i.e up
void dorotate_y(GLfloat angl) {

	int indx;

	if (num == 0) {
		for (int i = 0; i < 9; i++) {
			indx = slots[i];
			rot[indx] = RotateY(angle * angl) * rot[indx];
		}
	} else if (num == 1) {
		for (int i = 9; i < 18; i++) {
			indx = slots[i];
			rot[indx] = RotateY(angle * angl) * rot[indx];
		}
	} else {
		for (int i = 18; i < 27; i++) {
			indx = slots[i];
			rot[indx] = RotateY(angle * angl) * rot[indx];
		}
	}

	glutPostRedisplay();
}

void finish_y() {
	int strt;

	if (num == 0) {
		strt = 0;
	} else if (num == 1) {
		strt = 9;
	} else {
		strt = 18;
	}

// reassign slots
	if (angle > 0) {
		//anti clockwise
		transpose(strt, 1);
		swap_rows(strt, 1);
	} else {
		//clockwise
		swap_rows(strt, 1);
		transpose(strt, 1);
	}
}

//rows are counted from 0-2 starting from -ve z direction i.e most left
void dorotate_z(GLfloat angl) {

	int indx;

	if (num == 0) {
		for (int j = 0; j < 3; j++) {
			for (int i = 0; i < 3; i++) {
				indx = slots[6 + j * 9 + i];
				rot[indx] = RotateZ(angle * angl) * rot[indx];
			}
		}
	} else if (num == 1) {
		for (int j = 0; j < 3; j++) {
			for (int i = 0; i < 3; i++) {
				indx = slots[3 + j * 9 + i];
				rot[indx] = RotateZ(angle * angl) * rot[indx];
			}
		}
	} else {
		for (int j = 0; j < 3; j++) {
			for (int i = 0; i < 3; i++) {
				indx = slots[j * 9 + i];
				rot[indx] = RotateZ(angle * angl) * rot[indx];
			}
		}
	}

	glutPostRedisplay();
}

void finish_z() {
	int strt;

// change direction
	if (num == 0) {
		strt = 6;
	} else if (num == 1) {
		strt = 3;
	} else {
		strt = 0;
	}

// reassign slots
	if (angle > 0) {
		//anti clockwise
		transpose(strt, 2);
		swap_rows(strt, 2);
	} else {
		//clockwise
		swap_rows(strt, 2);
		transpose(strt, 2);
	}
}

void randomize(int);

void simulate_rotatex(int value) {

	tmpa += anglest;

	if (tmpa >= 90.0) {
		dorotate_x(90.0 - (tmpa - anglest));
		tmpa = 0;
		finish_x();
		animate_d = false;
		if (random_d) {
			glutTimerFunc(rdelay, randomize, rdelay);
		} else {
			//check win
			check_win();
		}
	} else {
		dorotate_x(anglest);
		glutTimerFunc(value, simulate_rotatex, value);
	}
}

void simulate_rotatey(int value) {

	tmpa += anglest;

	if (tmpa >= 90.0) {
		dorotate_y(90.0 - (tmpa - anglest));
		tmpa = 0;
		finish_y();
		animate_d = false;
		if (random_d) {
			glutTimerFunc(rdelay, randomize, rdelay);
		} else {
			//check win
			check_win();
		}
	} else {
		dorotate_y(anglest);
		glutTimerFunc(value, simulate_rotatey, value);
	}
}

void simulate_rotatez(int value) {

	tmpa += anglest;

	if (tmpa >= 90.0) {
		dorotate_z(90.0 - (tmpa - anglest));
		tmpa = 0;
		finish_z();
		animate_d = false;
		if (random_d) {
			glutTimerFunc(rdelay, randomize, rdelay);
		} else {
			//check win
			check_win();
		}
	} else {
		dorotate_z(anglest);
		glutTimerFunc(value, simulate_rotatez, value);
	}
}

void randomize(int v) {
	int op = 0;

	angle = 1;
	bool entr = false;

	if (!animate_d) {
		entr = true;
		animate_d = true;

		// randomize
		num = rand() % 3;
		angle *= -1;
		op = rand() % 3;

		if (op == 0) {
			dorotate_x(0);
			simulate_rotatex(timemsec);
		} else if (op == 1) {
			dorotate_y(0);
			simulate_rotatey(timemsec);
		} else {
			dorotate_z(0);
			simulate_rotatez(timemsec);
		}
	}

	if (randm__cnt < rtimes) {
		if (entr) {
			randm__cnt++;
		}
	} else {
		random_d = false;
		animate_d = false;
	}
}

void keyboard(unsigned char key, int x, int y) {
	switch (key) {
	case 033: // Esc key
		exit( EXIT_SUCCESS);
		break;
	case 040: { // space bar
		mat4 newm;
		rotate = newm;
		animate_d = false;
		mvp = projection * view * rotate * model;
		glutPostRedisplay();
		break;
	}
	case 015: // enter key
		if (!random_d) {
			randm__cnt = 0;
			random_d = true;
			randomize(0);
		}
		break;
	case 's':
		if (!animate_d && !random_d) {
			// initialize slots
			for (int i = 0; i < 27; ++i) {
				slots[i] = i;
				mat4 tn;
				rot[i] = tn;
			}
			pause_d = false;
			glutPostRedisplay();
		}
		break;
	default: {
		if (!pause_d && !animate_d && !random_d) {
			if (glutGetModifiers() == GLUT_ACTIVE_ALT) {
				angle = 1; //anti clockwise
			} else {
				angle = -1; //clockwise
			}

			switch (key) {
			case '1':
				num = 0;
				animate_d = true;
				dorotate_x(0);
				simulate_rotatex(timemsec);
				break;
			case '2':
				num = 1;
				animate_d = true;
				dorotate_x(0);
				simulate_rotatex(timemsec);
				break;
			case '3':
				num = 2;
				animate_d = true;
				dorotate_x(0);
				simulate_rotatex(timemsec);
				break;
			case '4':
				num = 0;
				animate_d = true;
				dorotate_y(0);
				simulate_rotatey(timemsec);
				break;
			case '5':
				num = 1;
				animate_d = true;
				dorotate_y(0);
				simulate_rotatey(timemsec);
				break;
			case '6':
				num = 2;
				animate_d = true;
				dorotate_y(0);
				simulate_rotatey(timemsec);
				break;
			case '7':
				num = 0;
				animate_d = true;
				dorotate_z(0);
				simulate_rotatez(timemsec);
				break;
			case '8':
				num = 1;
				animate_d = true;
				dorotate_z(0);
				simulate_rotatez(timemsec);
				break;
			case '9':
				num = 2;
				animate_d = true;
				dorotate_z(0);
				simulate_rotatez(timemsec);
				break;
			}
		}
	}
	}
}

void dorotate(GLfloat anglex, GLfloat angley, GLfloat anglez) {

	rotate = RotateZ(anglez) * RotateY(angley) * RotateX(anglex) * rotate;

//	mat4 c;
//	c[0] = camera[0];
//	c[1] = camera[1];
//	c[2] = camera[2];
//	c[3] = camera[3];
//
//	c = RotateY(-angley) * c;
//	camera[0] = c[0][0];
//	camera[1] = c[1][0];
//	camera[2] = c[2][0];
//	camera[3] = c[3][0];

//	mat4 c;
//	c[0] = camera[0];
//	c[1] = camera[1];
//	c[2] = camera[2];
//	c[3] = camera[3];
//
//	c = RotateZ(-angley) * c;
////	rotate = RotateY(angley) * rotate;
//	camera[0] = c[0][0];
//	camera[1] = c[1][0];
//	camera[2] = c[2][0];
//	camera[3] = c[3][0];
//
//	view = LookAt(camera, // Camera is at (4,3,-3), in World Space
//			vec4(0, 0, 0, 1), // and looks at the origin
//			vec4(0, camera[2] < 0 ? -1 : 1, 0, 1) // Head is up (set to 0,-1,0 to look upside-down)
//					);

	mvp = projection * view * rotate * model;

	glutPostRedisplay();
}

void reshape(int w, int h) {
	window_width = w;
	window_height = h;
	glViewport(0, 0, (GLsizei) w, (GLsizei) h); // Set our viewport to the size of our window
}

void mouse_click(int button, int state, int x, int y) {
	if (button == 0 && state == 0) {
		drag_s_point = vec2(x, y);
	}
}

void mouse_motion(int x, int y) {
	int diffx = x - drag_s_point.x;
	int diffy = y - drag_s_point.y;

	if (glutGetModifiers() == GLUT_ACTIVE_CTRL)
		dorotate(0, 0, theta * diffx / factor);
	else
		dorotate(theta * diffy / factor, theta * diffx / factor, 0);

	drag_s_point.x = x;
	drag_s_point.y = y;
}

void special_keys(int key, int x, int y) {
	if (glutGetModifiers() == GLUT_ACTIVE_CTRL)
		switch (key) {
		case GLUT_KEY_LEFT:
			dorotate(0, 0, theta * factor);
			break;
		case GLUT_KEY_RIGHT:
			dorotate(0, 0, -theta * factor);
			break;
		}
	else
		switch (key) {
		case GLUT_KEY_UP:
			dorotate(theta * factor, 0, 0);
			break;
		case GLUT_KEY_DOWN:
			dorotate(-theta * factor, 0, 0);
			break;
		case GLUT_KEY_LEFT:
			dorotate(0, -theta * factor, 0);
			break;
		case GLUT_KEY_RIGHT:
			dorotate(0, theta * factor, 0);
			break;
		}
}

//----------------------------------------------------------------------------

int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(window_width, window_height);

//----------------------------------------
// If you are using freeglut, the next two lines will check if
// the code is truly 3.2. Otherwise, comment them out

// glutInitContextVersion( 3, 2 );
// glutInitContextProfile( GLUT_CORE_PROFILE );
//----------------------------------------

	glutCreateWindow("Rubik's Cube");

	glewInit();

	init();

	glutDisplayFunc(display);
	glutMotionFunc(mouse_motion);
	glutMouseFunc(mouse_click);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(special_keys);
	glutReshapeFunc(reshape);

	glutMainLoop();
	return 0;
}
